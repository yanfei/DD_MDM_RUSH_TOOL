/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.metadata;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zhangxu
 * 
 * 关于主数据的元数据，描述一个主数据的基本信息和属性信息
 * 
 * 结构：
 *  默认主数据有三个必选属性，ID，name和code。所有属性都放在属性list里面，包括ID，name，code
 * 
 * 
 * 
 */
public class MainDataMetaDataEntity {
  
    
    
    //元数据ID
    String ID;
    //主数据名称
    String mainDataName;
    //主数据包含的副本源有多少个。（本不应该放到这里，但是不是道放到哪里，先放这里吧）
    int carbonDataCount;
    //英文名称
    String enName;

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }
    
    
    private List<MainDataPropertyEntity> properties = defaultPropertities();
    
     public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMainDataName() {
        return mainDataName;
    }

    public void setMainDataName(String mainDataName) {
        this.mainDataName = mainDataName;
    }

    public int getCarbonDataCount() {
        return carbonDataCount;
    }

    public void setCarbonDataCount(int carbonDataCount) {
        this.carbonDataCount = carbonDataCount;
    }

    public List<MainDataPropertyEntity> getProperties() {
        
        return properties;
    }

    public void setProperties(List<MainDataPropertyEntity> properties) {
        this.properties = properties;
    }
    
    /**
     * 每个主数据中都含有三个固定字段
     * 
     * 分别是id，name，和code
     * @return 
     */
    private List<MainDataPropertyEntity> defaultPropertities(){
        
        List<MainDataPropertyEntity> dlist = new ArrayList();
        //id
        MainDataPropertyEntity id = new MainDataPropertyEntity();
        id.setDatatype(MainDataPropertyEntity.DT_STRING);
        id.setLength(20);
        id.setName("id");
        id.setNullable(false);
        id.setMdp_id("");
        dlist.add(id);
        
        //name
        MainDataPropertyEntity name = new MainDataPropertyEntity();
        name.setDatatype(MainDataPropertyEntity.DT_STRING);
        name.setLength(50);
        name.setName("name");
        name.setNullable(false);
        name.setMdp_id("");
        dlist.add(name);
        
        //code
        MainDataPropertyEntity code = new MainDataPropertyEntity();
        code.setDatatype(MainDataPropertyEntity.DT_STRING);
        code.setLength(50);
        code.setName("code");
        code.setNullable(true);
        code.setMdp_id("");
        dlist.add(code);
        
        return dlist;
    }
    
    //校验函数
    public boolean valedata(){
        return true;
    }
}
