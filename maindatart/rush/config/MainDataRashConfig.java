/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package maindatart.rush.config;

import java.util.List;

/**
 *  清洗指标设定类
 * 
 * 1、设置主数据每个字段在对比中所占的比重关系。比如主数据中有五个字段，默认每个字段都占20%。建议管理员设置每个字段所占比重。比重高的就在判断中占有
 * 更重要的位置。所有比重加在一起为1.
 * 
 * 3、设置相似比例在多少以上就判断数据一致。
 * 
 * 
 * @author 丁丁
 */
public class MainDataRashConfig {
    
    //表主键
    String id;
    //对应的主数据名字
    String mainDataName;
    //比这个值高就算两个主数据一样
    double same;
    //比这个值低，就说明两个组数据不一样
    double different;
    //对每个属性的配置，包括每个属性所占一致性的比例，因为有的属性比较重要，有的属性不那么重要。还有每种属性对应的算法，不同的属性应当对应不同的算法
    List<PropertityConfig> itemConfig;
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainDataName() {
        return mainDataName;
    }

    public void setMainDataName(String mainDataName) {
        this.mainDataName = mainDataName;
    }

    public double getSame() {
        return same;
    }

    public void setSame(double same) {
        this.same = same;
    }

    public double getDifferent() {
        return different;
    }

    public void setDifferent(double different) {
        this.different = different;
    }

    public List<PropertityConfig> getItemConfig() {
        return itemConfig;
    }

    public void setItemConfig(List<PropertityConfig> itemConfig) {
        this.itemConfig = itemConfig;
    }
   
     //校验函数
    public boolean valedata(){
        return true;
    }
    
}
