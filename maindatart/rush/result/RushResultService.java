/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.result;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import maindatart.maindata.MainDataBean;
import maindatart.maindata.MainDataContener;
import maindatart.maindata.MainDataService;
import maindatart.metadata.MDMetaDataService;
import maindatart.metadata.MainDataMetaDataEntity;
import maindatart.metadata.MainDataPropertyEntity;

/**
 *
 * @author 丁丁
 * 
 * 清洗结果管理service
 * 
 */
public class RushResultService {
    
    RushResultDao dao = new RushResultDao();
    
    /**
     * 记录比较后“相同”的数据
     * @param pde
     * @throws java.lang.Exception
     */
    public void  addSameLog(PendingDataEntity pde) throws Exception{
        pde.setType(PendingDataEntity.SANE);
        try {
            dao.insertRushPendingData(pde);
        } catch (SQLException ex) {
            Logger.getLogger(RushResultService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    /**
     * 记录比较厚“相似”的数据
     * @param pde
     * @throws java.lang.Exception
     */
    public void addLikeLog(PendingDataEntity pde)throws Exception{
        pde.setType(PendingDataEntity.LIKE);
        try {
            dao.insertRushPendingData(pde);
        } catch (SQLException ex) {
            Logger.getLogger(RushResultService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    /**
     * 记录一次清洗
     * @param rre
     * @throws java.lang.Exception
     */
    public void recordRush(RushReportEntity rre) throws Exception{
        try {
            dao.insertRushResult(rre);
        } catch (SQLException ex) {
            Logger.getLogger(RushResultService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    public List<RushReportEntity> getRushReportEntitylist(String mainDataName)throws Exception{
        return dao.getRushReportEntitylist(mainDataName);
    }
    
    public RushReportEntity getRushReportEntity(String id)throws Exception{
        return dao.getByID(id);
    }
    
    public List<PendingDataEntity> getPendingDatas(String id)throws Exception{
        return dao.getPendingDatasDesc(id,PendingDataEntity.NOT_DELL_STATE);
    }
    
    public void dellPendingDatas(String id)throws Exception{
        dao.upDatePendingDataDt(id,PendingDataEntity.DELL_STATE);
    }
    /**
     * 将清洗结果写入excel文件中
     * @param filename
     * @param rushRecordID
     * @throws Exception 
     */
    public void writeInExcelFile(String filename,String rushRecordID)throws Exception{
        
        MainDataService mdService = new MainDataService();
        MDMetaDataService mdmdService = new MDMetaDataService();
        
        RushReportEntity rushReportEntity = this.getRushReportEntity(rushRecordID);
        
        MainDataMetaDataEntity mdmd = mdmdService.getMainDataMetaDataByName(rushReportEntity.getMainDataName());
        
        List<PendingDataEntity> pendingDatas = getPendingDatas(rushRecordID);
       
        //对应集合
        Map<PendingDataEntity,List<PendingDataEntity>> map = new HashMap();
        
        //把按照乙方相同的标准将数据合并
        for(PendingDataEntity pde : pendingDatas){
            List<PendingDataEntity> list = new ArrayList();
            list.add(pde);
            map.put(pde, list);
            
            for(int i = pendingDatas.indexOf(pde) + 1; i < pendingDatas.size();i ++){
                PendingDataEntity other = pendingDatas.get(i);
                if(pde.getMaindataID2() == null && other.getMaindataID2() == null && pde.getMaindataID2().equals(other.getMaindataID2())){
                    list.add(other);
                    pendingDatas.remove(other);
                    System.out.println("合并了一条");
                }
            }
        }
        //将结果输出到excel文件中
        WritableWorkbook wwb = Workbook.createWorkbook(new File(filename));
        WritableSheet ws = wwb.createSheet("排重结果", 0);
        int line = 0;
        List<MainDataPropertyEntity> properties = mdmd.getProperties();
        for(int i = 0;i < properties.size();i ++){
            MainDataPropertyEntity p = properties.get(i);
            Label labelC = new Label(i + 1,line, p.getName());
            ws.addCell(labelC);
        }
        line ++;
        Set<PendingDataEntity> keySet = map.keySet();
        
        for(PendingDataEntity pde : keySet){
           String maindataid = pde.getMaindataID2();
           MainDataBean mdb = mdService.getMainDatas(mdmd, maindataid);
           this.print(ws, line, mdmd, mdb.getMc(), "乙方");
           line ++;
           List<PendingDataEntity> list = map.get(pde);
           for(PendingDataEntity pdeSecond : list){
               String maindataidsecond = pde.getMaindataID1();
               MainDataBean mdbsecond = mdService.getMainDatas(mdmd, maindataidsecond);
               this.print(ws, line, mdmd, mdbsecond.getMc(), "甲方:相似度" + pdeSecond.getCompareValue());
               line ++;
           }
        }
        
        wwb.write();// 写入数据
        wwb.close();// 关闭文件
        
        
    }
    
    /**
     * 将一条主数据写入excel的指定页签中
     * @param ws
     * @param line
     * @param mdmd
     * @param mdc 
     */
    private void print(WritableSheet ws,int line,MainDataMetaDataEntity mdmd,MainDataContener mdc,String info) throws WriteException{

        Label fristLable = new Label(0,line, info);
        ws.addCell(fristLable);
        
        List<MainDataPropertyEntity> properties = mdmd.getProperties();
           for(int i = 0;i < properties.size();i ++){
                MainDataPropertyEntity p = properties.get(i);
                String propertyname = p.getName();
                Label labelC = new Label(i + 1, line, (String)mdc.getValue(propertyname));
                ws.addCell(labelC);
           }
    }
    
    public static void main(String[] args) throws IOException, WriteException{
        try{
            WritableWorkbook wwb = Workbook.createWorkbook(new File("c:/ss.xls"));
            WritableSheet ws = wwb.createSheet("排重结果", 0);
            Label labelC = new Label(0, 0, "This is a Label cell");
            ws.addCell(labelC);
            wwb.write();// 写入数据
            wwb.close();// 关闭文件

        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
       
    }
}
