package maindatart.rush.result;

/**
 * 
 * @author zhangxu
 * 
 * 扫描结果报告明细
 * 
 * 扫描报告有两种
 * 	1、数据不一致性报告。即主数据中有记录，可以副本中没有。
 * 		这种情况的判断比较复杂。当我们认为主数据和副本数据是体积相当的时候，也就是说，主数据中有了，但是副本数据中没有（而且是应该有的）的时候，
 * 		生成该报告。提示副本数据源中应该加入该数据
 * 		如何判定这个主数据应该在副本数据中有：
 * 		a）事先就知道两个数据应当是一致的。
 * 		b）副本数据是主数据的一部分。且缺失数据在这一部分中
 * 		c）人工判断生成的
 * 2、数据项差异报告
 * 		报告主数据与副本数据之间的数据差异。通知副本数据源进行修改
 *
 */
public class RushReportEntity {
	
	//报告ID
	private String Id;
	
	//报告内容
	private String info;	
	//主数据名称
	private String mainDataName;	
	//算法ID
        private String algName;

        private String time;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getAlgName() {
            return algName;
        }

        public void setAlgName(String algName) {
            this.algName = algName;
        }
	
	
	public String getMainDataName() {
		return mainDataName;
	}
	public void setMainDataName(String mainDataName) {
		this.mainDataName = mainDataName;
	}
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
}
