/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.maindata;

import java.util.List;
import maindatart.metadata.MainDataMetaDataEntity;

/**
 *
 * @author 丁丁
 * 
 * 主数据集成
 */
public class MainDataListBean {
 
    MainDataMetaDataEntity mdmd;
    
    List<MainDataContener> datas;

    public MainDataMetaDataEntity getMdmd() {
        return mdmd;
    }

    public void setMdmd(MainDataMetaDataEntity mdmd) {
        this.mdmd = mdmd;
    }

    public List<MainDataContener> getDatas() {
        return datas;
    }

    public void setDatas(List<MainDataContener> datas) {
        this.datas = datas;
    }
    
    
}
