package maindatart.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * 
 * @author 丁丁
 *
 *	集合转换类Util
 *
 */
public class CollectionUtil {

	public static <T> List<T> setToList(Set<T> set){
		
		List<T> list = new ArrayList<T>();
		Iterator<T> it = set.iterator();
		while(it.hasNext()){
			
			list.add(it.next());
		}
		
		
		return list;
	}
}
